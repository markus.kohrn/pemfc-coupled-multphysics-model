# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 17:57:57 2022

@author: kohrn
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

'''=== general imports ==='''
import matplotlib.pyplot   as     plt
from   matplotlib.gridspec import GridSpec
import numpy               as     np
import sys, os

'''=== user defined imports ==='''

# swith cwd to parent directory, if not yet done
if os.path.split(os.getcwd())[1] == "MultiPhysModel":
    os.chdir("..")

import MultiPhysModel.GxGy as GxGy
import MultiPhysModel.elch as elch


'''
===============================================================================   
        Heat Map: Currents
===============================================================================
'''
def currents(curX, curY, curReact, cur_bal_rel, p, winTitle):
    
    # extracting recurring parametres
    xElems, yElems, nSpecs = p.xElems, p.yElems, p.nSpecs
    iE, iI                 = p.iE, p.iI

    fig, ax = plt.subplots(2,3)
    fig.canvas.manager.set_window_title(winTitle)
    im = [0,0,0,0,0,0]
    ax = [ax[0,0], ax[0,1], ax[0,2], ax[1,0], ax[1,1], ax[1,2]]

# == preprocessing ==       
    area_xDiff_frame = np.ones(curX[:,:,:].shape)
    area_xDiff_frame[1:-1,1:-1,:] = np.repeat(p.area_xDiff[:,:,np.newaxis],nSpecs,axis=2)
    
    area_yDiff_frame = np.ones(curY[:,:,0].shape)
    area_yDiff_frame[1:-1,  0 ] = p.area_yDiff[:, 0]
    area_yDiff_frame[1:-1,1:-1] = p.area_yDiff
    area_yDiff_frame[1:-1, -1 ] = p.area_yDiff[:,-1]
    area_yDiff_frame = np.repeat(area_yDiff_frame[:,:,np.newaxis],nSpecs,axis=2)
    
    cur2X = curX / area_xDiff_frame /10**4 # A/cm2
    cur2Y = curY / area_yDiff_frame /10**4 # A/cm2
        
    cur2Vx  = np.zeros((xElems,  yElems,  nSpecs))
    cur2Vy  = np.zeros((xElems,  yElems,  nSpecs))
    cur2Vol = np.zeros((xElems,  yElems,  nSpecs))
    
    cur2Vy[ :  ,1:-1,: ]  = (cur2Y[:  , :-1,: ] + cur2Y[:,1:   ,: ] ) /2
    cur2Vx[1:-1,1:-1,: ]  = (cur2X[:-1,1:-1,: ] + cur2X[1:,1:-1,: ] ) /2
    cur2Vol               = np.sqrt(cur2Vx**2 + cur2Vy**2)
    cur2Vol[:,  0   ,: ]  = cur2Y[:  , 0,: ]
    cur2Vol[:, -1   ,: ]  = cur2Y[:  ,-1,: ]

# == ploting ==     
    im[0] = ax[0].imshow(cur_bal_rel[:,:,iE].transpose(), cmap='jet')
    im[1] = ax[1].imshow(cur2X[:,:,iE].transpose()      , cmap='jet')
    im[2] = ax[2].imshow(cur2Y[:,:,iE].transpose()      , cmap='jet')
    im[3] = ax[3].imshow(cur2Vol[:,:,iE].transpose()    , cmap='jet')
    im[4] = ax[4].imshow(curReact[:,:,iE].transpose()   , cmap='jet')
    im[5] = ax[5].imshow(curReact[:,:,iI].transpose()   , cmap='jet')
    
    for idx in range(6):
        plt.colorbar(im[idx], ax=ax[idx], orientation="horizontal")
        ax[idx].invert_yaxis()
    
    ax[0].set_title("cur_bal_rel Electric [A/A]")
    ax[1].set_title("cur dens. X [A/cm2]")
    ax[2].set_title("cur dens. Y [A/cm2]")
    ax[3].set_title("Cur dens. abs [A/cm2]")
    ax[4].set_title("curReact Ion [A]")
    ax[5].set_title("curReact Elec [A]")

'''
===============================================================================   
        Heat Maps: Water Flows
===============================================================================
'''
def water(curX, curY, curReact, curPC_gl, curPC_gm, p, winTitle):

    # extracting recurring parametres
    xElems, yElems, nSpecs = p.xElems, p.yElems, p.nSpecs
    iWg, iWl, iWm          = p.iWg, p.iWl, p.iWm 

    fig, ax = plt.subplots(3,4)
    fig.canvas.manager.set_window_title(winTitle)
    im = np.zeros(ax.shape)
    im = im.tolist()

# == preprocessing ==       
    area_xDiff_frame = np.ones(curX[:,:,:].shape)
    area_xDiff_frame[1:-1,1:-1,:] = np.repeat(p.area_xDiff[:,:,np.newaxis],nSpecs,axis=2)
    
    area_yDiff_frame = np.ones(curY[:,:,0].shape)
    area_yDiff_frame[1:-1,  0 ] = p.area_yDiff[:, 0]
    area_yDiff_frame[1:-1,1:-1] = p.area_yDiff
    area_yDiff_frame[1:-1, -1 ] = p.area_yDiff[:,-1]
    area_yDiff_frame = np.repeat(area_yDiff_frame[:,:,np.newaxis],nSpecs,axis=2)
    
    cur2X = curX / area_xDiff_frame /10**4 # A/cm2
    cur2Y = curY / area_yDiff_frame /10**4 # A/cm2
        
    cur2Vx  = np.zeros((xElems,  yElems,  nSpecs))
    cur2Vy  = np.zeros((xElems,  yElems,  nSpecs))
    cur2Vol = np.zeros((xElems,  yElems,  nSpecs))
    
    cur2Vy[ :  ,1:-1,: ]  = (cur2Y[:  , :-1,: ] + cur2Y[:,1:   ,: ] ) /2
    cur2Vx[1:-1,1:-1,: ]  = (cur2X[:-1,1:-1,: ] + cur2X[1:,1:-1,: ] ) /2
    cur2Vol               = np.sqrt(cur2Vx**2 + cur2Vy**2)
    cur2Vol[:,  0   ,: ]  = cur2Y[:  , 0,: ]
    cur2Vol[:, -1   ,: ]  = cur2Y[:  ,-1,: ]

# == ploting ==
    for iWx, iPlot in zip([iWg, iWl, iWm],[0,1,2]):
        im[0][iPlot] = ax[0,iPlot].imshow(cur2X[  :,:,iWx].transpose(), cmap='jet')
        im[1][iPlot] = ax[1,iPlot].imshow(cur2Y[  :,:,iWx].transpose(), cmap='jet')
        im[2][iPlot] = ax[2,iPlot].imshow(cur2Vol[:,:,iWx].transpose(), cmap='jet')

    im[0][3] = ax[0,3].imshow(curReact[:,:,iWg].transpose(), cmap='jet')
    im[1][3] = ax[1,3].imshow(curPC_gl[:,:,iWg].transpose(), cmap='jet')
    im[2][3] = ax[2,3].imshow(curPC_gm[:,:,iWg].transpose(), cmap='jet')
    
    for iPlot in range(3):
        for jPlot in range(4):
            plt.colorbar(im[iPlot][jPlot], ax=ax[iPlot,jPlot], orientation="horizontal")
            ax[iPlot,jPlot].invert_yaxis()
    
    ax[0,0].set_title("W.Gas cur dens. X [mol/cm2.s]")
    ax[1,0].set_title("W.Gas cur dens. Y [mol/cm2.s]")
    ax[2,0].set_title("W.Gas cur dens. abs [mol/cm2.s]")

    ax[0,1].set_title("W.Liquid cur dens. X [mol/cm2.s]")
    ax[1,1].set_title("W.Liquid cur dens. Y [mol/cm2.s]")
    ax[2,1].set_title("W.Liquid cur dens. abs [mol/cm2.s]")
    
    ax[0,2].set_title("W.Membrane Cur dens. X [mol/cm2.s]")
    ax[1,2].set_title("W.Membrane Cur dens. Y [mol/cm2.s]")
    ax[2,2].set_title("W.Membrane Cur dens. abs [mol/cm2.s]")    
    
    ax[0,3].set_title("W.Gas react cur dens. abs [mol/cm3.s]")
    ax[1,3].set_title("W.Gas->liquid cur dens. abs [mol/cm3.s]")   
    ax[2,3].set_title("W.Gas->membrane cur dens. abs [mol/cm3.s]")
    
    fig.tight_layout()
    


'''
===============================================================================   
        Lines/Heatmap: Water Distribution
===============================================================================
'''

def water2(pot, p, winTitle="water 2"):

    # extracting recurring parametres
    geoDict                = p.geoDict
    iWg, iWl, iWm          = p.iWg, p.iWl, p.iWm 
    
    fig = plt.figure()
    fig.canvas.manager.set_window_title(winTitle)
    
    gs = GridSpec(2,6,figure=fig)
    ax00 = fig.add_subplot(gs[0,0:3])
    ax01 = fig.add_subplot(gs[0,3:6])
    ax10 = fig.add_subplot(gs[1,0:2])
    ax11 = fig.add_subplot(gs[1,2:4])
    ax12 = fig.add_subplot(gs[1,4:6])
    ax = [ax00, ax01, ax10, ax11, ax12]

    yLenSumEdge   = np.array([-geoDict["yLenElems"][0]]  # 33
                        +[0]
                        +[sum(geoDict["yLenElems"][0:i+1]) for i in range(geoDict["yElemsMEA"])]
                        +[sum(geoDict["yLenElems"]) + geoDict["yLenElems"][-1] ] )    
    yLenSumCenter = (yLenSumEdge[:-1] + yLenSumEdge[1:] )/2  # 32   
    
#=================
    act = GxGy.activity_from_cWg(pot,          p)
    lam = GxGy.lambda_from_cWm(  pot[:,:,iWm], p)
    sat = GxGy.poreSat_H2Ol(     pot[:,:,iWl], p)
    
    lineStyle_list = [".-", ".--"]
        
    for xCoord in [1,-2]:
        lineStyle = lineStyle_list[xCoord]
        
        ax[0].plot(yLenSumCenter, pot[xCoord,:,iWg], lineStyle, color="g", label="gas")
        ax[0].plot(yLenSumCenter, pot[xCoord,:,iWl], lineStyle, color="b", label="liquid")
        ax[0].plot(yLenSumCenter, pot[xCoord,:,iWm], lineStyle, color="r", label="membrane")

        ax[1].plot(yLenSumCenter, act[xCoord,:], lineStyle, color="g", label="gas")
        ax[1].plot(yLenSumCenter, sat[xCoord,:], lineStyle, color="b", label="liquid")
        ax[1].plot(yLenSumCenter, lam[xCoord,:], lineStyle, color="r", label="membrane")
     
    for idx in range(2):
        ax[idx].legend()       

#=================
    im =[None]*5
    
    im[2] = ax[2].imshow(act.transpose(), cmap='jet')
    im[3] = ax[3].imshow(lam.transpose(), cmap='jet')
    im[4] = ax[4].imshow(sat.transpose(), cmap='jet')
    
    for idx in range(2,5):
        plt.colorbar(im[idx], ax=ax[idx], orientation="horizontal")
        ax[idx].invert_yaxis()

#================= 
    title_list =["Water concentration [mol/m3]", "Water activity/lambda/saturation [-]",
                 "Gas: Activity [p/p_sat]", "Membrane: loading", "Pores: Saturation"]
    for idx in range(5):
        ax[idx].set_title(title_list[idx])


'''
===============================================================================   
        Heatmap: potential/concentrations
===============================================================================
'''

def concentrations(pot, p, winTitle):
    
    # extracting recurring parametres
    xElems, yElems, nSpecs = p.xElems, p.yElems, p.nSpecs
    iH2, iO2, iN2          = p.iH2, p.iO2, p.iN2
    iWg, iWl, iWm          = p.iWg, p.iWl, p.iWm 
    
    fig, ax = plt.subplots(2,3)
    fig.canvas.manager.set_window_title(winTitle)
    im = [0]*(nSpecs-2)
    ax = [ax[0,0], ax[0,1], ax[0,2], ax[1,0], ax[1,1], ax[1,2]]
    titles = ["H2", "O2", "N2", "H2O (g)", "H2O (l)", "H2O (m)"]
    im[0] = ax[0].imshow(pot[:,:,iH2].transpose(), cmap='jet')
    im[1] = ax[1].imshow(pot[:,:,iO2].transpose(), cmap='jet')
    im[2] = ax[2].imshow(pot[:,:,iN2].transpose(), cmap='jet')
    im[3] = ax[3].imshow(pot[:,:,iWg].transpose(), cmap='jet')
    im[4] = ax[4].imshow(pot[:,:,iWl].transpose(), cmap='jet')
    im[5] = ax[5].imshow(pot[:,:,iWm].transpose(), cmap='jet')
    for idx in range(nSpecs-2):
        plt.colorbar(im[idx], ax=ax[idx], orientation="horizontal")
        ax[idx].invert_yaxis()
        ax[idx].set_title(titles[idx]+" concentration")

'''
===============================================================================   
        Lineplot: Vol/Cur/Bal/React
===============================================================================
'''

def linePlot(pot, curX, curY, curReact, cur_bal_rel, 
             E_eq_CLa, E_eq_CLc, U_CLa, U_CLc, geoDict, p, winTitle):
    
    iE, iI = p.iE, p.iI
    
    fig, ax = plt.subplots(2,2)
    fig.canvas.manager.set_window_title(winTitle)
    ax = [ax[0,0],ax[0,1],ax[1,0],ax[1,1]]
    
    ix = 1
    
    yLenSumEdge   = np.array([-geoDict["yLenElems"][0]]  # 33
                            +[0]
                            +[sum(geoDict["yLenElems"][0:i+1]) for i in range(geoDict["yElemsMEA"])]
                            +[sum(geoDict["yLenElems"]) + geoDict["yLenElems"][-1] ] )
    yLenSumCenter = (yLenSumEdge[:-1] + yLenSumEdge[1:] )/2  # 32
    yLenSumDiff   = yLenSumEdge[1:-1]   # 31
    
    E_th_a = np.zeros(U_CLa[ix,:].shape) + elch.E_th("HOR", p)
    E_th_c = np.zeros(U_CLc[ix,:].shape) + elch.E_th("ORR", p)
    
    barWidth = 0.8 * min(geoDict["yLenElems"])
        
    ax[0].set_title("Voltage [V]")
    ax[0].plot(yLenSumCenter, pot[ix,:,iE], ".-")
    ax[0].plot(yLenSumCenter, pot[ix,:,iI], ".-")
    
    ax[1].set_title("Current [A]")
    ax[1].plot(    yLenSumDiff[       0  : p.yPEM_0 ], curY[ix,       0  : p.yPEM_0 ,p.iE], ".-")   
    ax[1].plot(    yLenSumDiff[p.yCLa_0-1:p.yGDLc_0 ], curY[ix,p.yCLa_0-1:p.yGDLc_0 ,p.iI], ".-")   
    ax[1].plot(    yLenSumDiff[p.yCLc_0-1:p.yElems-1], curY[ix,p.yCLc_0-1:p.yElems-1,p.iE], ".-")
    ax[1].scatter( yLenSumCenter,                      curReact[ix,:,iE])
    ax[1].scatter( yLenSumCenter,                      curReact[ix,:,iI])
    
    ax[2].set_title("Balance rel. (DEL_cur/mean_cur) [-]")
    ax[2].plot(yLenSumCenter[p.yV_ELa], cur_bal_rel[ix,p.yV_ELa,iE], ".-")
    ax[2].plot(yLenSumCenter[p.yV_ION], cur_bal_rel[ix,p.yV_ION,iI], ".-")
    ax[2].plot(yLenSumCenter[p.yV_ELc], cur_bal_rel[ix,p.yV_ELc,iE], ".-")
    
    ax[3].set_title("Voltage [V]")
    ax[3].bar(yLenSumCenter[p.yCLa_0:p.yPEM_0 ], U_CLa[   ix,:], width=barWidth    )
    ax[3].bar(yLenSumCenter[p.yCLc_0:p.yGDLc_0], U_CLc[   ix,:], width=barWidth    )
    ax[3].bar(yLenSumCenter[p.yCLa_0:p.yPEM_0 ], E_eq_CLa[ix,:], width=barWidth*0.6)
    ax[3].bar(yLenSumCenter[p.yCLc_0:p.yGDLc_0], E_eq_CLc[ix,:], width=barWidth*0.6)
    ax[3].bar(yLenSumCenter[p.yCLa_0:p.yPEM_0 ], E_th_a        , width=barWidth*0.3)
    ax[3].bar(yLenSumCenter[p.yCLc_0:p.yGDLc_0], E_th_c        , width=barWidth*0.3)

    for i in range(4):
        # horizontal lines
        ax[i].axhline(0                     , color="grey", linestyle="--")
        # vertical lines        
        ax[i].axvline(yLenSumDiff[        0  ], color="grey", linestyle="--")
        ax[i].axvline(yLenSumDiff[ p.yCLa_0-1], color="grey", linestyle="--")
        ax[i].axvline(yLenSumDiff[ p.yPEM_0-1], color="grey", linestyle="--")
        ax[i].axvline(yLenSumDiff[ p.yCLc_0-1], color="grey", linestyle="--")
        ax[i].axvline(yLenSumDiff[p.yGDLc_0-1], color="grey", linestyle="--")
        ax[i].axvline(yLenSumDiff[         -1], color="grey", linestyle="--")
        # limits
        ax[i].set_xlim(xmin=min(yLenSumCenter), xmax=max(yLenSumCenter))
        
    ax[3].set_xlim(xmin=p.d_GDLa, xmax=p.d_GDLa+p.d_CLa+p.d_PEM+p.d_CLc)


'''
===============================================================================   
         Plot: Resistances
===============================================================================
'''

def conductances(Gx, Gy, geoDict, p, winTitle):
    
    fig, ax = plt.subplots(4,4)
    fig.canvas.manager.set_window_title(winTitle)
    
    im = [[0]*4,[0]*4,[0]*4,[0]*4] 
    titles = [["Gx El", "Gx Ion", "Gx H2", "Gx O2"],["Gx N2", "Gx H2O (g)", "Gx H2O (l)", "Gx H2O (m)"],
              ["Gy El", "Gy Ion", "Gy H2", "Gy O2"],["Gy N2", "Gy H2O (g)", "Gy H2O (l)", "Gy H2O (m)"]]
    
    for iSpec in range(4):
        im[0][iSpec] = ax[0,iSpec].imshow(Gx[:,:,iSpec  ].transpose(), cmap='jet')       
        im[1][iSpec] = ax[1,iSpec].imshow(Gx[:,:,iSpec+4].transpose(), cmap='jet') 
        im[2][iSpec] = ax[2,iSpec].imshow(Gy[:,:,iSpec  ].transpose(), cmap='jet')
        im[3][iSpec] = ax[3,iSpec].imshow(Gy[:,:,iSpec+4].transpose(), cmap='jet')
      
    xVol_bounds = [1]
    xVol_bounds.append(xVol_bounds[-1] + geoDict["yElemsGDLa"])
    xVol_bounds.append(xVol_bounds[-1] + geoDict["yElemsCLa"] )
    xVol_bounds.append(xVol_bounds[-1] + geoDict["yElemsPEM"] )
    xVol_bounds.append(xVol_bounds[-1] + geoDict["yElemsCLc"] )
    xVol_bounds.append(p.yElems-1)
 
    hLine_list     = [0,0]
    hLine_list[0] = [x-0.5 for x in xVol_bounds]
    hLine_list[1] = [x-1.0 for x in xVol_bounds]
    
    for i in range(4):
        for j in range(4):
            plt.colorbar(im[i][j], ax=ax[i,j], orientation="vertical")
            ax[i,j].invert_yaxis()
            #ax[i,j].hlines(hLine_list[i], *ax[i,j].get_xlim()) 
            ax[i,j].set_title(titles[i][j]) 
    
     
    fig.tight_layout()
    
    return True

#==============================================================================
def conductances2(Gx, Gy, geoDict, p, winTitle):
    
    fig, ax = plt.subplots(2,p.nSpecs)
    fig.canvas.manager.set_window_title(winTitle)
    
    im = [[0]*p.nSpecs,[0]*p.nSpecs] 
    titles = [["Gx El", "Gx Ion", "Gx H2", "Gx O2", "Gx N2", "Gx H2O (g)", "Gx H2O (l)", "Gx H2O (m)"],
              ["Gy El", "Gy Ion", "Gx H2", "Gy O2", "Gy N2", "Gy H2O (g)", "Gy H2O (l)", "Gy H2O (m)"]]
    
    for iSpec in range(p.nSpecs):
        im[0][iSpec] = ax[0,iSpec].imshow(Gx[:,:,iSpec].transpose(), cmap='jet')    
        im[1][iSpec] = ax[1,iSpec].imshow(Gy[:,:,iSpec].transpose(), cmap='jet')
      
    xVol_bounds = [1]
    xVol_bounds.append(xVol_bounds[-1] + geoDict["yElemsGDLa"])
    xVol_bounds.append(xVol_bounds[-1] + geoDict["yElemsCLa"] )
    xVol_bounds.append(xVol_bounds[-1] + geoDict["yElemsPEM"] )
    xVol_bounds.append(xVol_bounds[-1] + geoDict["yElemsCLc"] )
    xVol_bounds.append(p.yElems-1)
 
    hLine_list    = [0,0]
    hLine_list[0] = [x-0.5 for x in xVol_bounds]
    hLine_list[1] = [x-1.0 for x in xVol_bounds]
    
    for i in range(2):
        for j in range(p.nSpecs):
            plt.colorbar(im[i][j], ax=ax[i,j], orientation="horizontal")
            ax[i,j].invert_yaxis()
            ax[i,j].hlines(hLine_list[i], *ax[i,j].get_xlim()) 
            ax[i,j].set_title(titles[i][j]) 
    
     
    fig.tight_layout()
    
    return True

'''
===============================================================================   
        Plot: Error Progress
===============================================================================
'''

def errorProgress(curReact_list, curPC_gm_list, curPC_gl_list, curEOD_list, 
                       curBalRel_MeanMax_list, p,
                       curReact_list_a,curReact_list_c,
                       winTitle="Error Progression"):

    curReact_arr = np.array(curReact_list)
    curPC_gm_arr = np.array(curPC_gm_list)
    curPC_gl_arr = np.array(curPC_gl_list)
    curEOD_arr   = np.array(curEOD_list)
    resArray = np.array(curBalRel_MeanMax_list)
    
    fig = plt.figure()
    fig.canvas.manager.set_window_title(winTitle)
    
    gs = GridSpec(2,4,figure=fig)
    ax0 = fig.add_subplot(gs[0,0])
    ax1 = fig.add_subplot(gs[0,1])
    ax2 = fig.add_subplot(gs[0,2])
    ax3 = fig.add_subplot(gs[0,3])
    ax4 = fig.add_subplot(gs[1,0:2])
    ax5 = fig.add_subplot(gs[1,2:4])
    ax = [ax0,ax1,ax2,ax3,ax4,ax5]
    
    # sub-figures 0-3: current evolution
    label_list   = ["react"     , "gas -> membrane", "MIN: gas -> liquid", "EOD"]
    cur_arr_list = [curReact_arr, curPC_gm_arr     , curPC_gl_arr        , curEOD_arr]
    
    for iAx, label, cur_X_arr in zip(range(4), label_list, cur_arr_list):
        ax[iAx].plot(cur_X_arr[:,0], label="MIN: "+label)
        ax[iAx].plot(cur_X_arr[:,1], label="MEAN:"+label)
        ax[iAx].plot(cur_X_arr[:,2], label="MAX: "+label)

      
    # sub-figures 4,5: error progression    
    spec_list = ["E","I","H2","O2","N2","Wg","Wl","Wm"]
    threshold_list = [p.mp_threshold_curBalRel_Mean, p.mp_threshold_curBalRel_Max]
    label_list     = ["Mean", "Max"]
    
    for idx in range(2):
        for iSpec in range(p.nSpecs):
            ax[idx+4].plot(resArray[:,idx,iSpec], label="%s rel. %s" %(label_list[idx], spec_list[iSpec]))       
        
        ax[idx+4].set_yscale('log')
        ax[idx+4].axhline(threshold_list[idx], color="grey", linestyle="--")    
    
    ax[4].plot(np.mean(resArray[:,0,:], axis=1), label="mean.", color="grey", linestyle="--")
    ax[5].plot(np.max( resArray[:,1,:], axis=1), label="max.",  color="grey", linestyle="--")
            
    # all figures    
    title_list = ["reaction current rate","molar flow rate H2O(g) -> H2O(m)",
                  "molar flow rate H2O(g) -> H2O(l)", "electro-osmotic drag rate",
                  "relative flow error (mean)","relative flow error (max)"]
    
    for idx in range(len(ax)):
        ax[idx].legend()
        ax[idx].set_title(title_list[idx])

#==============================================================================
#   Plot  2

    curReact_arr_a =  np.array(curReact_list_a) / p.d_CLa
    curReact_arr_c = -np.array(curReact_list_c) / p.d_CLc

    fig, ax = plt.subplots()
    fig.canvas.manager.set_window_title("current progression")
    
    ax.plot(curReact_arr_a[:,0], label="A:MIN: ", color="b")
    ax.plot(curReact_arr_a[:,1], label="A:MEAN:", color="b")
    ax.plot(curReact_arr_a[:,2], label="A:MAX: ", color="b")

    ax.plot(curReact_arr_c[:,0], label="C:MIN: ", color="r")
    ax.plot(curReact_arr_c[:,1], label="C:MEAN:", color="r")
    ax.plot(curReact_arr_c[:,2], label="C:MAX: ", color="r")
    
    ax.legend()




#==============================================================================


    
    return True

'''
===============================================================================   
        Plot: Current Balance
===============================================================================
'''

def currentBalance(cur_bal, cur_bal_rel, p):
    
    titles         = ["Elec", "Ion", "H2", "O2", "N2", "H2O (g)", "H2O (l)", "H2O (m)"]
    title_appendix = [" - cur.Balance", " - rel.Error"]
    winTitles      = ["Current balance","Relativ Error"]
    dataArrays     = [cur_bal, cur_bal_rel]
    
    xVol_bounds = [1]
    xVol_bounds.append(xVol_bounds[-1] + p.geoDict["yElemsGDLa"])
    xVol_bounds.append(xVol_bounds[-1] + p.geoDict["yElemsCLa"] )
    xVol_bounds.append(xVol_bounds[-1] + p.geoDict["yElemsPEM"] )
    xVol_bounds.append(xVol_bounds[-1] + p.geoDict["yElemsCLc"] )
    xVol_bounds.append(p.yElems-1)         
    hLine_list = [x-0.5 for x in xVol_bounds] 
    
    for iArray in range(len(dataArrays)):
        dataArray = dataArrays[iArray]
    
        fig, ax = plt.subplots(2,4)
        ax = [ax[0,0],ax[0,1],ax[0,2],ax[0,3],ax[1,0],ax[1,1],ax[1,2],ax[1,3]]
        im = [0]*p.nSpecs
        fig.canvas.manager.set_window_title(winTitles[iArray])
        
        for iX in range(p.nSpecs):
            im[iX] = ax[iX].imshow(dataArray[:,:,iX].transpose(), cmap='jet')
            plt.colorbar(im[iX], ax=ax[iX], orientation="horizontal")
            ax[iX].invert_yaxis()
            ax[iX].hlines(hLine_list, *ax[iX].get_xlim()) 
            ax[iX].set_title(titles[iX] + title_appendix[iArray])
 
    return True

'''
===============================================================================   
        Plot: U-I-Characteristic Curve
===============================================================================
'''

def UIchar(UIchar_dict_list, p, winTitle=""):
    
    fig, axL = plt.subplots() 
    fig.canvas.manager.set_window_title(winTitle)   
    
    axR = axL.twinx()
    
    axL.set_xlabel('current density i" / A/cm2')
    axL.set_ylabel('cell voltage U / V'      )
    axR.set_ylabel('power density p" / W/cm2') 
    
    for UIchar_dict in UIchar_dict_list:
    
        U_cell    = np.array(UIchar_dict["U_cell"   ])
        i2_cm2    = np.array(UIchar_dict["i2_cm2"   ])
        OCV_fract = np.array(UIchar_dict["OCV_fract"])
        p2_cm2    = i2_cm2 * U_cell
    
        axL.plot(i2_cm2, U_cell, '.-', label=UIchar_dict["label"])
        axR.plot(i2_cm2, p2_cm2, '.--')   
    
        xAnno = i2_cm2 + (np.max(i2_cm2)-np.min(i2_cm2)) * 0.01
        yAnno = U_cell + (np.max(U_cell)-np.min(U_cell)) * 0.01
        
        for i, OCV_fract in enumerate(OCV_fract):
            
            axL.annotate(r"$f_{\rm{OCV}}$ = %d %%" %(OCV_fract*100), (xAnno[i], yAnno[i]))
                   
    axL.set_ylim(ymin=0)
    axR.set_ylim(ymin=0)
    axL.set_xlim(xmin=0)
    axL.legend()
    
    return True

    
    
    
    
    
    