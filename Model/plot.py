# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 14:24:21 2021

@author: Markus Kohrn
         RWTH Aachen University, Germany        

"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt

                       
'''
===============================================================================
                    Imports
===============================================================================
'''


''' import standard libraries'''                   
from contextlib import contextmanager
import os, sys, io, tempfile
import math
import numpy             as np
import matplotlib.pyplot as plt
import matplotlib        as mpl
import multiprocessing   as mp
import copy
import time
from   scipy             import optimize
from   scipy.interpolate import interp1d
from   scipy.ndimage     import gaussian_filter1d

''' imoprt own function classes '''

import MultiPhysModel.MP_main as MP_main
import MultiPhysModel.plot    as MP_plot
import MultiPhysModel.plot_paper as MP_plot_paper
import AnalytModel.AM2_main   as AM_main
import AnalytModel.AM2_plot   as AM_plot
import class_database         as c_db


'''
===============================================================================
                    Color sets etc.
===============================================================================
'''
 

# RWTH color list
color_list12 = ["#00549F", "#E30066", "#FFED00", "#006165", "#0098A1",
                "#57AB27", "#BDCD00", "#F6A800", "#CC071E", "#A11035", 
                "#612158", "#7A6FAC"]
color_list11 = ["#00549F", "#E30066", "#FFED00", "#006165", "#0098A1",
                "#57AB27", "#BDCD00", "#F6A800", "#CC071E", "#A11035", 
                "#612158"]
color_list6  = ["#00549F", "#E30066", "#FFED00", "#006165", "#0098A1",
                "#57AB27"]
                
mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=color_list11) 


DO_SUPPRESS_PRINT = True
DO_SUPPRESS_PLOTS = True

'''
===============================================================================
                    plot functions
===============================================================================
'''




def NEW_plot():
    
    dataBase = c_db.database(createNewDatabase=False)
    
    UIchar_dict_list = list()
    
    # loop over all stored results    
    for paraObj, setIdx, R_load_list in dataBase.dataList:
            
        # load result data
        paraObj, _, AMres, MPresList, _ = dataBase.load_resultFile(setIdx)

    
    #================================================        
        # loop over MPresList
        for OCV_fract, MPres in MPresList:
            
            if MPres == False:
                # unconverged result
                continue
            
            p        = paraObj
            pot      = MPres["pot"]
            winTitle = "OCV_fract = %.2f" %(MPres["OCV_fract"])
            
            MP_plot.concentrations(pot, p, winTitle)


    #================================================         
        U_list, i_list, f_list = list(), list(), list()

        # loop over MPresList
        for OCV_fract, MPres in MPresList:
            
            if MPres == False:
                # unconverged result
                continue
        
            U_list.append( MPres["pot"][1,-1,paraObj.iE]                             ) # V
            i_list.append( (abs(MPres["cur2_CLa_cm2"])+abs(MPres["cur2_CLc_cm2"]))/2 ) # A/cm2
            f_list.append( OCV_fract                                                 ) # -
    
        fiU_list = sorted(zip(f_list, i_list, U_list))
        
        UIchar_dict = {"OCV_fract"  : [f for f,_,_ in fiU_list], 
                        "i2_cm2"     : [i for _,i,_ in fiU_list], 
                        "U_cell"     : [U for _,_,U in fiU_list],
                        "label"      : r"idx = %d" %(setIdx),       }
        
        if len(f_list) != 0:
            UIchar_dict_list.append(UIchar_dict)
        
    #MP_plot.UIchar(UIchar_dict_list, paraObj, winTitle="R_RIB %d" %R_RIB)
    MP_plot.UIchar(UIchar_dict_list, paraObj, winTitle="p_tot_a")


'''
===============================================================================
                    Start-up Routine
===============================================================================
'''
if __name__ == "__main__":
    NEW_plot()