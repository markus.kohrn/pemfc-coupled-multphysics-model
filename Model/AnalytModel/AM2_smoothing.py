# -*- coding: utf-8 -*-
"""
Created on Fri Dec 23 15:20:50 2022

@author: Marku
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals
                 
import numpy as np
import copy, math
import matplotlib.pyplot as plt
import time


# %matplotlib qt

'''
===============================================================================
           main method
===============================================================================
'''


def main(allRegs, trueStiff, doPlot=True, doPlotAdditional=True):
    
    # set parameters
    loopsMax = allRegs.paraObj.sm_loopsMax
    f_adjust = allRegs.paraObj.sm_f_adjust
    th_Fmean = allRegs.paraObj.sm_threshold_Fmean
    th_Fmax  = allRegs.paraObj.sm_threshold_Fmax
    
    # create ind
    ind = create_ind(allRegs, trueStiff)
        
    # calc initial h, s, e
    calc_init_hse(ind)
    
    # run loop:
    doSmooth(ind, loopsMax, f_adjust, th_Fmean, th_Fmax, doPlotAdditional)
    
    # prepare return map
    maps_sm = {"h" : ind["h"],
               "u" : ind["u"],
               "s" : ind["s"],
               "t" : ind["t"],
               "e" : ind['e'],
               "x" : ind['x'],
               "y" : ind['y']
              }

    # print initial and final
    if doPlot:
        plot_lines(ind, mode="initial")
        plot_lines(ind, mode="final")
    
    # return statement
    return maps_sm


'''
===============================================================================
           do smooth
===============================================================================
'''    

def doSmooth(ind, loopsMax, f_adjust, th_Fmean, th_Fmax, doPlot):

    # extracting data
    
    y           = ind["y" ]
    E           = ind["Ey"]
    G_interX_fr = ind["G_interX_fr"]
    E_interY_fr = ind["Ey_interY_fr"]
    k           = ind["k" ]
    
    xNodes    = ind["xNodes"]
    yNodes    = ind["yNodes"]
    
    xLen      = ind["xLen"]
    yLen_node = ind["yLen_node"]
    xLen_node = ind["xLen_node"]
    
    ix_rib_B = ind["ix_rib_B" ]
    ix_rib_T = ind["ix_rib_T" ]
    ix_cha_B = ind["ix_cha_B" ]
    ix_cha_T = ind["ix_cha_T" ]
    
    y_2D = np.repeat(y[np.newaxis,:], xNodes, axis=0)
    
#==============================================================================
# initialize loop  
  
    h          = ind["h"]
    e          = ind["e"]
    Force      = np.zeros(h.shape)    
    time_start = time.time()
    converged  = False
    
    # start loop
    for loop in range(loopsMax):

#==============================================================================    
# Manipulate h
          
        h[:,:] +=  Force[:,:] * yLen_node[:,:] * f_adjust

#==============================================================================
# calc forces

    #========================
    #   shear induced force

        dhdx_interX_fr         = np.zeros((xNodes+1,  yNodes))
        dhdx_interX_fr[1:-1,:] = np.diff(h, axis=0) / xLen
        
        dtdx   = (  dhdx_interX_fr[1:  ,:] * G_interX_fr[1:  ,:] 
                  - dhdx_interX_fr[ :-1,:] * G_interX_fr[ :-1,:]  ) / xLen_node * k
        
        DEL_t  = dtdx  * xLen_node
        
        tForce = DEL_t * yLen_node # * (1+ey))
        
        
    #========================
    #   normal strain force
               
        e_interY = np.diff(h, axis=1) / np.diff(y_2D, axis=1) - 1
        
        s_interY_fr                = np.zeros((xNodes, yNodes+1))
        
        s_interY_fr[    :   ,1:-1] = e_interY * E_interY_fr[:,1:-1]
        
        s_interY_fr[ix_rib_B,  0 ] = s_interY_fr[ix_rib_B, 1] + tForce[ix_rib_B, 0] / xLen_node[ix_rib_B, 0]
        s_interY_fr[ix_rib_T, -1 ] = s_interY_fr[ix_rib_T,-2] - tForce[ix_rib_T,-1] / xLen_node[ix_rib_T,-1]
        
        s_interY_fr[ix_cha_B,  0 ] = - s_interY_fr[ix_cha_B,  1 ]
        s_interY_fr[ix_cha_T, -1 ] = - s_interY_fr[ix_cha_T, -2 ]        

        DEL_s  = np.diff(s_interY_fr, axis=1)

        sForce = DEL_s * xLen_node
    
    #========================
    #   total force
         
        Force = sForce + tForce

#==============================================================================
#   calc e
    
        s = (s_interY_fr[:,:-1] + s_interY_fr[:,1:]) /2
        e = s / E

#==============================================================================
# converge, print & plot
        
        if loop in [0,1,2] or (loop+1) % 100 == 0:
        
            # check for convergence
            if (  np.mean(np.abs(Force)) < th_Fmean
              and np.max( np.abs(Force)) < th_Fmax  ):
                converged = True
    
            # print progressin
            if converged or loop==0 or (loop+1)%50000==0:
                print("Loop:%7d - %3d%% -%4ds - Force[mean/max]=%7.4f / %7.4f" 
                      %(loop+1, (loop+1)/loopsMax*100, time.time()-time_start , 
                        np.mean(np.abs(Force)), np.max(np.abs(Force)) ) )
            
            # update dict AND plot
            if converged or loop in [0,1,2] or (loop+1)%int(loopsMax/5)==0:              
                
                # update dict
                ind.update({"h"     : h,
                            "u"     : h - y,
                            "s"     : s,
                            "t"     : t_from_h(h, xLen, k, G_interX_fr),
                            "e"     : e,
                            "sForce": sForce,
                            "tForce": tForce,
                            "Force" : Force    })              
                # plot
                if doPlot:
                    plot(ind, loop)
            
            # early exit
            if converged:
                print("-> smoothing converged")
                break


'''
===============================================================================
           create ind
===============================================================================
'''
    
def create_ind(allRegs, trueStiff):
                                 
#==============================================================================
# extract data from regime dict
    
    pD = allRegs.pD
    gD = allRegs.paraObj.geoDict

    x  = allRegs.maps_hom['x'] 
    y  = allRegs.maps_hom['y'] 
    
    h0 = allRegs.maps_hom['h']
    u0 = allRegs.maps_hom['u']
    s0 = allRegs.maps_hom['s']
    t0 = allRegs.maps_hom['t']
    
    Ey = pD["Ey"]
    G  = pD["G" ]
    k  = pD["k" ]
           
    xNodes, yNodes = s0.shape
    
    def create_ix_cha_node(ElemsIsCha): #================================================
        # as soon as one of the elements is gap -> node is gap                          #
        NodeIsCha = np.zeros(xNodes, dtype=bool)                                        #
        NodeIsCha[  0 ] = ElemsIsCha[ 0]                                                #
        NodeIsCha[1:-1] = ElemsIsCha[ :-1] & ElemsIsCha[1:] # L:rib OR R:rib -> rib     #
        NodeIsCha[ -1 ] = ElemsIsCha[-1]                                                #
        ix_cha_node = np.arange(xNodes)[NodeIsCha]                                      #
        return ix_cha_node #=============================================================
         
    ix_cha_B = create_ix_cha_node(gD["xElemsIsCHAa"])
    ix_cha_T = create_ix_cha_node(gD["xElemsIsCHAc"])
    
    ix_all   = np.arange(xNodes)
    
    ix_rib_B = ix_all[~np.isin(ix_all, ix_cha_B)]
    ix_rib_T = ix_all[~np.isin(ix_all, ix_cha_T)]

#==============================================================================
# create Ey array

    def create_modulusArr(EyG, trueStiff): #======================
                                                                 #
        M           = np.zeros((xNodes,   yNodes  ))             #  
        M_interX_fr = np.zeros((xNodes+1, yNodes  ))             #
        M_interY_fr = np.zeros((xNodes,   yNodes+1))             #
                                                                 #
        # inter y values                                         #
        if trueStiff:                                            #
            if EyG=="Ey":                                        #
                M_gdl = allRegs.paraObj.Ey_GDL  *10**(-6)        #
                M_cl  = allRegs.paraObj.Ey_CL   *10**(-6)        #
                M_pem = allRegs.paraObj.Ey_PEM  *10**(-6)        #
            elif EyG=="G":                                       # 
                M_gdl = allRegs.paraObj.Gxy_GDL *10**(-6)        #
                M_cl  = allRegs.paraObj.Gxy_CL  *10**(-6)        #
                M_pem = allRegs.paraObj.Gxy_PEM *10**(-6)        #
                                                                 #
            y0 = 0                                               #
            y1 = y0 + gD["yElemsGDLa"] +1                        #
            y2 = y1 + gD["yElemsCLa" ]                           #
            y3 = y2 + gD["yElemsPEM" ]                           #
            y4 = y3 + gD["yElemsCLc" ]                           #
            y5 = y4 + gD["yElemsGDLc"] +1                        #
                                                                 #
            M_interY_fr[:,y0:y1] = M_gdl   # 10+1 - GDLa + frame #
            M_interY_fr[:,y1:y2] = M_cl    #  3   - CLa          #
            M_interY_fr[:,y2:y3] = M_pem   #  6   - PEM          #
            M_interY_fr[:,y3:y4] = M_cl    #  6   - CLc          #
            M_interY_fr[:,y4:y5] = M_gdl   # 10+1 - GDLc + frame #
                                                                 #
        else:                                                    #
            M_interY_fr[:,:] = pD[EyG]                           #
                                                                 #
        # Nodal Values                                           #
        M[:,:] = ( M_interY_fr[:,:-1] + M_interY_fr[:,1:] ) /2   #
                                                                 #
        # inter x values                                         #
        M_interX_fr[:,:] = M[0,:]                                #
                                                                 #
        return M, M_interX_fr, M_interY_fr #======================
    
    
    Ey, Ey_interX_fr, Ey_interY_fr = create_modulusArr("Ey", trueStiff)
    G,  G_interX_fr,  G_interY_fr  = create_modulusArr("G",  trueStiff)


#==============================================================================
# create between nodes and lengths of elements    
   
    xLen = np.zeros((xNodes-1,yNodes  ))
    yLen = np.zeros((xNodes,  yNodes-1))
    
    xLen = np.repeat( (x[1:] - x[:-1])[:,np.newaxis], yNodes, axis=1)
    yLen = np.repeat( (y[1:] - y[:-1])[np.newaxis,:], xNodes, axis=0)
    

    xLen_node = list()
    xLen_node.append(  (x[1]  - x[0]  )  )
    xLen_node.extend( [(x[i+1]- x[i-1]) /2 for i in range(1,len(x)-1)] )
    xLen_node.append(  (x[-1] - x[-2] )  )
    xLen_node = np.array(xLen_node)
    xLen_node = np.repeat(xLen_node[     :    ,np.newaxis], yNodes, axis=1)


    yLen_node = list()
    yLen_node.append(  (y[1]  - y[0]  ) /2 )
    yLen_node.extend( [(y[i+1]- y[i-1]) /2 for i in range(1,len(y)-1)] )
    yLen_node.append(  (y[-1] - y[-2] ) /2 )
    yLen_node = np.array(yLen_node)    
    yLen_node = np.repeat(yLen_node[np.newaxis,     :    ], xNodes, axis=0)


#==============================================================================
# initialize individual
    
    ind   = {"h0"       : h0,        "h"           : None,
             "u0"       : u0,        "u"           : None,
             "s0"       : s0,        "s"           : None,
             "t0"       : t0,        "t"           : None,
             "e0 "      : s0/Ey,     "e"           : None,
                               
             "Ey"       : Ey,        "Ey_interX_fr": Ey_interX_fr, "Ey_interY_fr": Ey_interY_fr,    
             "G"        : G,         "G_interX_fr" : G_interX_fr,  "G_interY_fr" : Ey_interY_fr,         
             "k"        : k,
             
             "x"        : x,         "y"           : y,
             "xNodes"   : xNodes,    "yNodes"      : yNodes,
             "xLen"     : xLen,      "yLen"        : yLen,
             "xLen_node": xLen_node, "yLen_node"   : yLen_node,
             
             "ix_cha_B" : ix_cha_B,  "ix_cha_T"    : ix_cha_T,
             "ix_rib_B" : ix_rib_B,  "ix_rib_T"    : ix_rib_T,
             
             "sForce"   : None,      "tForce"      : None,        "Force"       : None,
            }
    
    return ind

    

'''
===============================================================================
# creating an inital homogeniouse strain distribution
===============================================================================
'''
 
def calc_init_hse(ind):
    
    Ey        = ind["Ey"]
    h0        = ind["h0"]
    y         = ind["y"]
    yNodes    = ind["yNodes"]
    yLen_node = ind["yLen_node"]
    yLen      = ind["yLen"]
    
    e_mean = ( (h0[:,-1] - h0[:,0])  / (y[-1] - y[0])  ) - 1                   # x-vector
    E_mean = 1 / np.sum( 1/Ey[0,:] * yLen_node[0,:] / np.sum(yLen_node[0,:]) ) # scalar   
    s_mean = E_mean * e_mean                                                   # x-vector
    
    s_new  = np.repeat(s_mean[:,np.newaxis], yNodes, axis=1)                   # x-y-matrix
    e_new  = s_new / Ey
    h_new  = h_from_e(e_new, h0, yLen)
    
    ind.update({"h" : h_new,
                "s" : s_new,
                "e" : e_new  })
    
    return True


'''
===============================================================================
           helper
===============================================================================
'''

def h_from_e(e, h0, yLen):
    
    h        = np.repeat(h0[:,0][:,np.newaxis], h0.shape[1], axis=1)
    h[:,1:] += np.cumsum( ((e[:,:-1]+e[:,1:])/2 +1) * yLen , axis=1)
    
    return h

'''
===============================================================================
'''

def t_from_h(h, xLen, k, G_interX_fr):
    
    xNodes, yNodes = h.shape
    
    dhdx_interX_fr         = np.zeros((xNodes+1,  yNodes)) 
    dhdx_interX_fr[1:-1,:] = np.diff(h, axis=0) / xLen
    
    t_interX_fr = dhdx_interX_fr *k *G_interX_fr
    
    t = ( t_interX_fr[:-1,:] + t_interX_fr[1:,:] ) / 2
    
    return t

'''
===============================================================================
           plot
===============================================================================
'''
  
    
def plot(ind, loop):
       
    fig, ax = plt.subplots(3,2)
    fig.canvas.manager.set_window_title("loop=%6d" %(loop+1))
    
    ax = [ax[0,0],ax[1,0],ax[2,0],ax[0,1],ax[1,1],ax[2,1]]
    im = [None]*len(ax)
    
    h      = ind["h"]
    e      = ind["e"]
    u      = ind["u"]
    sForce = ind["sForce"]
    tForce = ind["tForce"]
    Force  = ind["Force"]
    
    im[0] = ax[0].imshow(sForce.T, cmap="jet")
    im[1] = ax[1].imshow(tForce.T, cmap="jet")
    im[2] = ax[2].imshow( Force.T, cmap="jet")

    im[3] = ax[3].plot(h)
    im[4] = ax[4].plot(u)
    im[5] = ax[5].plot(e)
    
    yLabel_list = ["normal force", "shear force" , "total force",
                   "height"      , "displacement", "strain"     ]
    
    for i in range(len(ax)):            
        ax[i].set_ylabel(yLabel_list[i])
 
        if i > 2: continue
    
        ax[i].invert_yaxis()
        plt.colorbar(im[i], ax=ax[i], orientation="vertical")
        
    fig.tight_layout()
  
    
'''
===============================================================================
'''


def plot_lines(ind, mode):
    
    data_init = [ ind["h0"], ind["u0"], ind["s0"], ind["t0"] ]
    data_fin  = [ ind["h"],  ind["u"],  ind["s"],  ind["t"]  ]
    
    # select data set
    if   mode == "initial": data = data_init
    elif mode == "final"  : data = data_fin
    else:                   return False
    
    # initialize plot
    fig, ax = plt.subplots(2,2)
    fig.canvas.manager.set_window_title(mode)
    ax = [ax[0,0],ax[0,1],ax[1,0],ax[1,1]]
    
    # set title and limits
    yLabel = [ r"hight $h$ / $\rm{\mu}$m",
               r"displacement  $u$ / $\rm{\mu}$m",
               r"normal stress $\sigma_{\rm{y}}$ / MPa",
               r"shear stress $\tau_{\rm{xy}}$ / MPa"]    
    
    # assign to subplots
    for idx in range(len(ax)):
      
        yItv = max(data_init[idx].max(), data_fin[idx].max()) - min(data_init[idx].min(), data_fin[idx].min())
        yMin = min(data_init[idx].min(), data_fin[idx].min()) - yItv * 0.05
        yMax = max(data_init[idx].max(), data_fin[idx].max()) + yItv * 0.05
        
        ax[idx].plot(ind["x"], data[idx])
        
        ax[idx].set_xlabel(r"length $x$ / $\rm{\mu}$m")
        ax[idx].set_ylabel(yLabel[idx])
        ax[idx].set_xlim(xmin=0, xmax=ind["x"][-1])
        ax[idx].set_ylim(ymin=yMin, ymax=yMax)
    
    return True

'''
===============================================================================
           start-up statement
===============================================================================
'''
# if __name__ == "__main__":
#     res = load("data")
#     main(res)

print(">> reloaded!!!")