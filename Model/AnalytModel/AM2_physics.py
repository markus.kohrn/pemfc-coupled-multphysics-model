# -*- coding: utf-8 -*-
"""
Created on Fri Jan 28 15:19:38 2022

@author: kohrn
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt

''' import standard libraries'''
import math
import numpy                 as     np
import matplotlib.pyplot     as     plt
from   matplotlib.animation  import FuncAnimation
import matplotlib            as     mpl
import copy
import time
from   scipy                 import optimize
from   scipy.interpolate     import interp1d
from   scipy.ndimage.filters import gaussian_filter1d



'''
===============================================================================
                   Calculate Maps based on Analytical solution
===============================================================================
'''

def calc_map_h(allRegs):
    "returns map of MEA hight" 
    h_arr      = allRegs.maps_smoothed['h']
    cBar_label = r"hight $h$ / $\mu$m"
    return h_arr, cBar_label

def calc_map_u(allRegs):
    "returns map of MEA displacement"
    u_arr      = allRegs.maps_smoothed['u']
    cBar_label = r"displacement $u$ / $\mu$m"
    return u_arr, cBar_label

def calc_map_s(allRegs):
    "returns map of MEA normal y-stress" 
    s_arr      = allRegs.maps_smoothed['s']
    cBar_label = r"normal stress $\sigma$ / MPa"
    return s_arr, cBar_label

def calc_map_t(allRegs):
    "returns map of MEA shear stress" 
    t_arr      = allRegs.maps_smoothed['t']
    cBar_label = r"shear stress / MPa"  
    return t_arr, cBar_label

def calc_map_e(allRegs):
    "returns map of MEA normal y-strain"
    pD         = allRegs.pD
    e_arr      = allRegs.maps_smoothed['s'] / pD['Ey']
    cBar_label = r"strain $\varepsilon$ / -"
    return e_arr, cBar_label

def calc_map_por(allRegs):
    "returns map of MEA porositys"
    pD         = allRegs.pD
    e_arr, _   = calc_map_e(allRegs)
    por_arr    = 1 - (1 - pD['por0']) / (1 + e_arr) 
    
    # TODO: membrane
    xElems, yElems = por_arr.shape
    yMid = int(round(yElems/2))
    por_arr[:, yMid-1:yMid+1] = 0
    
    cBar_label = r"porosity $p$ / -"
    return por_arr, cBar_label

def calc_map_elCond(allRegs):
    "returns map of MEA volumetric electric conductivity" 
    pD         = allRegs.pD
    m_Brug     = 1.5 # Bruggeman exponent
    por_arr, _ = calc_map_por(allRegs)
    elCond_arr = pD['elCon_s'] * (1 - por_arr)**m_Brug
    
    # TODO: volumetric resistivity of membrane
    xElems, yElems = por_arr.shape
    yMid = int(round(yElems/2))
    elCond_arr[:, yMid-1:yMid+1] = 0.2 *10**(-4)  # 0.2 S/cm = 0.00002 S/µm

    cBar_label = r"electric conductivity $\gamma$ / S/$\mu$m"    
    return elCond_arr, cBar_label

def calc_map_elRes(allRegs):
    "returns map of MEA electric resistivity" 
    elCond_arr,_= calc_map_elCond(allRegs)
    Rvol_arr    = 1/elCond_arr # /10**(8)
    cBar_label  = r"volume resistivity $\rho$ / $\Omega\mu$m"
    return Rvol_arr, cBar_label

'''
===============================================================================
     Calculate Maps based on Numerical interpolation - electric pot field
===============================================================================
'''

def calc_map_elPot(allRegs):
    "returns map of MEA electric potential"
    
    if "elPot" in allRegs.maps_phys:
        print("electric potential field is already known")
        pot_arr    = allRegs.maps_phys['elPot']
        cBar_label = r"electric potential $\Phi$ / V"    
        return pot_arr, cBar_label

    # Setup electric potential
    V_bot, V_top = 0, 1    
    
    # getting Number of elements in x and y
    xElems, yElems = allRegs.maps_hom['h'].shape

    # setting up potential field (=result array)
    pot_arr = np.zeros([xElems, yElems])
    pot_botRow  = [V_bot] * xElems
    pot_topRow  = [V_top] * xElems
    pot_arr     = np.column_stack([pot_botRow,  pot_arr,  pot_topRow])
    yElems += 2
 
    # Getting the Resistor-arrays RX, RY
    RX_arr, RY_arr = calc_RX_RY_arrs(allRegs)

    # calculate initial potential field (y-balance only)
    for ix in range(xElems):       
        # total resistance in y-column
        RY_tot = np.sum(RY_arr[ix,:])    
        
        for iy in range(1,yElems-1):
            pot_arr[ix, iy] = (V_top - V_bot) * RY_arr[ix,iy-1]/RY_tot + pot_arr[ix,iy-1]    

    # creation of slices
    iw = slice(0,xElems-2); i = slice(1,xElems-1); ie = slice(2,xElems) # x-direction
    js = slice(0,yElems-2); j = slice(1,yElems-1); jn = slice(2,yElems) # y-direction
    
    # loop over steps
    for loopCount in range(10000):
        
        # deepcopy of original array
        if loopCount % 500 == 0:
            pot_arr_orig = copy.deepcopy(pot_arr)
        
        # calculation of next step
        pot_arr[i,j] = (  RX_arr[iw,j] / (RX_arr[iw,j] + RX_arr[i,j]) * (pot_arr[ie,j] - pot_arr[iw,j]) + pot_arr[iw,j]
                        + RY_arr[i,js] / (RY_arr[i,js] + RY_arr[i,j]) * (pot_arr[i,jn] - pot_arr[i,js]) + pot_arr[i,js]
                       ) /2        
        
        # is end reached?
        if loopCount % 500 == 0:
            diff = np.average(np.absolute(pot_arr_orig - pot_arr))
            print("loop: %4d, diff= %f" %(loopCount, diff))
            
            if diff < 0.000001:
                break
    
#    # cut-out y-surface lines (BPP)        
#    pot_arr = pot_arr[:,1:-1]
        
    # safe map to all regs
    allRegs.maps_phys.update({"elPot": pot_arr})
    
    # set c-bar label
    cBar_label = r"electric potential $\Phi$ / V"    
    
    return pot_arr, cBar_label

#==============================================================================

def calc_map_curDens(allRegs, axis):
    "returns map of MEA current density distribution"

    # gettin ginput arrays/vectors
    pot_arr, _                     = calc_map_elPot(allRegs)
    RX_arr, RY_arr                 = calc_RX_RY_arrs(allRegs)
    botContact_vec, topContact_vec = calc_BPPcontact_vecs(allRegs)
    
    # getting Number of elements in x and y
    xElems, yElems = pot_arr.shape
    
    # lenght of one element
    lenElemX = allRegs.xMax()         / (xElems-1)
    lenElemY = allRegs.pD['H_mea']    / (yElems-1)

    # curDens array
    curDensX_arr = np.zeros([xElems, yElems])
    curDensY_arr = np.zeros([xElems, yElems])
    
    print("shape pot_arr = " +str(pot_arr.shape))  # 1001, 53  
    print("shape RX_arr  = " +str(RX_arr.shape) )  # 1000, 53   
    print("shape RY_arr  = " +str(RY_arr.shape) )  # 1001, 52
    
    for ix in range(xElems): # 0-1000
        for iy in range(yElems): # 0-52
            I_e, I_w, I_s, I_n = 0, 0, 0, 0
            nonZeroX, nonZeroY = 0, 0 
            
            # current in x-direction
            if ix != 0:         
                I_e = (pot_arr[ix,iy] - pot_arr[ix-1,iy]) / RX_arr[ix-1,iy]
                nonZeroX += 1
            if ix != xElems-1:  
                I_w = (pot_arr[ix,iy] - pot_arr[ix+1,iy]) / RX_arr[ix,iy]
                nonZeroX += 1
            
            # current in y-direction
            if iy != 0        and not (iy == 1        and botContact_vec[ix] == 0):         
                I_s = (pot_arr[ix,iy] - pot_arr[ix,iy-1]) / RY_arr[ix,iy-1]
                nonZeroY += 1
            if iy != yElems-1 and not (iy == yElems-2 and topContact_vec[ix] == 0): 
                I_n = (pot_arr[ix,iy] - pot_arr[ix,iy+1]) / RY_arr[ix,iy]
                nonZeroY += 1

            curDensX_arr[ix,iy] = (abs(I_e) + abs(I_w)) /nonZeroX / lenElemY * lenElemX
            curDensY_arr[ix,iy] = (abs(I_s) + abs(I_n)) /nonZeroY / lenElemX * lenElemY
  
    # return for x or y axis
    if axis == "x":
        cBar_label = r"El. current density in X $i''$ / A/$\mu$m$^2$"
        return curDensX_arr, cBar_label        
    elif axis == "y":
        cBar_label = r"El. current density in Y $i''$ / A/$\mu$m$^2$"
        return curDensY_arr, cBar_label
    elif axis == "tot":
        cBar_label = r"El. current density (total) $i''$ / A/$\mu$m$^2$"
        curDensTot_arr = np.sqrt(curDensX_arr**2 + curDensY_arr**2)
        return curDensTot_arr, cBar_label

#==============================================================================

def calc_map_R(allRegs, axis):
    "returns map of Resistors in x/y axis direction"
    
    RX_arr, RY_arr = calc_RX_RY_arrs(allRegs)
    
    if axis == "x":
        cBar_label = "El. resistence in X $R$ / $\Omega$"
        return RX_arr, cBar_label
    elif axis == "y":
        cBar_label = "El. resistence in Y $R$ / $\Omega$"
        return RY_arr, cBar_label    
    else:
        print("Flag error!")
        return False        
        
'''
===============================================================================
     Calculate supporting arrays and vectors
===============================================================================
''' 

def calc_RX_RY_arrs(allRegs):

    # Get Bot and Top surface resistance vectors
    Rsurf_bot, Rsurf_top = calc_vec_Rsurf(allRegs) # => Größenordnung 5*10^5 Ohm/mym [OK]    
   
    # Get volumetric resistivity array
    Rvol_arr,_ = calc_map_elRes(allRegs)

    # Nr of elements in x and y
    xElems, yElems = Rvol_arr.shape
  
    # lenght of one element
    lenElemX = allRegs.xMax()      / (xElems-1)
    lenElemY = allRegs.pD['H_mea'] / (yElems-1)
 
    # creation of X-Resistance array
    RX_arr = np.zeros([xElems-1, yElems+2])
    RX_arr[:, 0   ] = [0]*(xElems-1)
    RX_arr[:, 1:-1] = (Rvol_arr[:-1,:] + Rvol_arr[1:,:])/2 / lenElemY * lenElemX
    RX_arr[:,   -1] = [0]*(xElems-1)
   
    # creation of Y-Resistance array
    RY_arr = np.zeros([xElems, yElems+1])
    RY_arr[:, 0   ] = Rsurf_bot                            / lenElemX
    RY_arr[:, 1:-1] = (Rvol_arr[:,:-1] + Rvol_arr[:,1:])/2 / lenElemX * lenElemY
    RY_arr[:,   -1] = Rsurf_top                            / lenElemX
   
    return RX_arr, RY_arr


#==============================================================================    

def calc_BPPcontact_vecs(allRegs):
  
    # Get Bot and Top x-separation point lists
    xSepBot_list, xSepTop_list = allRegs.xSep_list()
    # add infinit element to list, to prevent for-loop from breaking
    xSepBot_list.append(np.inf)
    xSepTop_list.append(np.inf)

    # lenght of one element
    xElems, yElems = allRegs.maps_hom['h'].shape
    lenElemX = allRegs.xMax() / (xElems-1)
 
    # initialte result vectors (all elements in contact)
    botContact_vec, topContact_vec = np.ones(xElems), np.ones(xElems)
    
    iBotCon, iTopCon = 0, 0
    
    # iterate over all elements in result vectors (except 1.element, which is always in contact)   
    for ix in range(1,xElems):
        xElemCenter = lenElemX * (ix + 0.5)
        
        # Bottom
        if xElemCenter >= xSepBot_list[iBotCon]:
            # switch value (0->1, 1->0)
            botContact_vec[ix] = abs(botContact_vec[ix-1] - 1)
            # increase iBotCon
            iBotCon +=1
        else:
            botContact_vec[ix] = botContact_vec[ix-1]
            
        # Top
        if xElemCenter >= xSepTop_list[iTopCon]:
            # switch value (0->1, 1->0)
            topContact_vec[ix] = abs(topContact_vec[ix-1] - 1)
            # increase iBotCon
            iTopCon +=1
        else:
            topContact_vec[ix] = topContact_vec[ix-1]           
    # return statement
    return botContact_vec, topContact_vec

#==============================================================================

def calc_vec_Rsurf(allRegs):
    "returns bot and top vectors of surface resistance" 
   
    # calc supporting arrays/vectors
    s_arr, _                       = calc_map_s(allRegs)
    botContact_vec, topContact_vec = calc_BPPcontact_vecs(allRegs)
  
    # Interpolation dictionary for surface resistance f(y-stress)
    R_surf = {'p_clamp':[ +10.0, +0.001, -0.25, -0.35, -0.6, -0.85, -2.0, -2.5, -3.0, -3.5, -10.0],  # clamping press  in MPa
              'R_surf' :[   100,    100,    40,    30,   18,  14.5,    8,    7,    6,    5,     3]}  # surf resistence in mOhm/cm^2
            
    # Adjusting units for surface resistance -> Ohm/mym^2
    R_surf['R_surf'] = [R*10**5 for R in R_surf['R_surf']]
   
    # interpolation function for surface resistance
    f_Rsurf = interp1d(R_surf['p_clamp'],R_surf['R_surf'])
    
    # getting the shape of the map
    xElems, yElems = s_arr.shape
  
    # creating  surface resistance vactors
    Rsurf_bot = np.array( [ f_Rsurf(s_arr[i, 0]) for i in range(xElems) ] )
    Rsurf_top = np.array( [ f_Rsurf(s_arr[i,-1]) for i in range(xElems) ] )
   
    # correcting surface resistance vctors -> high resistance at air gap
    R_airGap = 100 *10**5
    for ix in range(len(Rsurf_bot)):
        if botContact_vec[ix] == 0:
            Rsurf_bot[ix] = R_airGap
        if topContact_vec[ix] == 0:
            Rsurf_top[ix] = R_airGap        
            

    # printing min and max stresses in map
#    print("sB_min = %f | sB_max = %f || sT_min = %f | sT_max = %f" %(s_arr[:, 0].min(),
#                                                                     s_arr[:, 0].max(),
#                                                                     s_arr[:,-1].min(),
#                                                                     s_arr[:,-1].max()))    

    # return statement
    return Rsurf_bot, Rsurf_top


'''
===============================================================================
     Main-funktion for plot heatmaps
===============================================================================
'''               

def plot_heatMap(allRegs, Del_h0, quantity, winTitle=None):
    
    pD = allRegs.pD
    
    maps_sm = allRegs.maps_smoothed
    
    # Safty check: Is there already a smoothed map?
    if maps_sm == None:
        print("Error: No smoothed maps yet. No physics map will be created!")
        return False
    
    # set-up min and max for colorbar    
    vmin, vmax = None, None
    y0 = 0
    
    # Case-select: Quantity
    if   quantity == "h":           # hight
        v_arr, cBar_label = calc_map_h(allRegs)
    elif quantity == "u":           # displacement
        v_arr, cBar_label = calc_map_u(allRegs)
    elif quantity == "s":           # normal stress
        v_arr, cBar_label = calc_map_s(allRegs)
    elif quantity == "t":           # shear stress
        v_arr, cBar_label = calc_map_t(allRegs)      
    elif quantity == "e":           # normal strain
        v_arr, cBar_label = calc_map_e(allRegs)      
    elif quantity == "por":         # porosity
        vmin, vmax = 0, 1
        v_arr, cBar_label = calc_map_por(allRegs)   
    elif quantity == "elCond":     # electric conductivity
        v_arr, cBar_label = calc_map_elCond(allRegs) 
    elif quantity == "elRes":      # electric resistivity
        v_arr, cBar_label = calc_map_elRes(allRegs)            
    elif quantity == "elPot":      # electric potential
        v_arr, cBar_label = calc_map_elPot(allRegs)
        vmin, vmax = 0, 1
        y0 = 1 
    elif quantity == "curDensX":
        v_arr, cBar_label = calc_map_curDens(allRegs, "x")
        vmin, vmax = 0, 3.5*10**(-6)
        y0 = 1 
    elif quantity == "curDensY":
        v_arr, cBar_label = calc_map_curDens(allRegs, "y")
        vmin, vmax = 0, 3.5*10**(-6)
        y0 = 1 
    elif quantity == "curDensTot":
        v_arr, cBar_label = calc_map_curDens(allRegs, "tot")
        vmin, vmax = 0, 3.5*10**(-6)
        y0 = 1 
    elif quantity == "RX_arr":
        v_arr, cBar_label = calc_map_R(allRegs, "x")
    elif quantity == "RY_arr":
        v_arr, cBar_label = calc_map_R(allRegs, "y")
    elif quantity == "th_cond":     # thermal conductivity
        return False
        
        
    # printing map    
    xElems, yElems = v_arr.shape
    
    v_arrT = np.transpose(v_arr)
    
    # lenght of one element
    lenElemX = allRegs.xMax()         / (xElems-1)
    lenElemY = allRegs.pD['H_mea']    / (yElems-1 - 2*y0)
    
    x_vec = np.linspace(0,              allRegs.xMax(), 21)
    y_vec = np.linspace(-pD['H_mea']/2, pD['H_mea']/2,  11)
    
    fig, ax = plt.subplots()
    im = ax.imshow(v_arrT, cmap='jet',  vmin=vmin, vmax=vmax)

    # setup for x-axis
    ax.set_xticks(x_vec/lenElemX)
    ax.set_xticklabels(x_vec)   
    ax.set_xlabel(r"in-plane / $\mu$m")
    
    # setup for  y-axis
    ax.set_yticks( y0 + (y_vec + y_vec[-1]) / lenElemY )
    ax.set_yticklabels(y_vec)
    ax.set_ylabel(r"through-plane / $\mu$m")
    ax.invert_yaxis()

    ax.set_aspect(xElems/yElems *1/5)
    
    fig.canvas.manager.set_window_title(winTitle)
    
    # Plot a colorbar with label.
    cBar = fig.colorbar(im, orientation="horizontal")
    cBar.set_label(cBar_label)
    
    return True

'''
===============================================================================
                   Calc clamping pressure
===============================================================================
'''
def calcClampingPress(allRegs, useSmoothedRes = False):
    
    # select maps
    if useSmoothedRes:
        maps = allRegs.maps_smoothed
    else: # use homogenized results
        maps = allRegs.maps_hom
    
    # saftety check    
    if maps == None:
        print("Error: selected flag [useSmoothedRes = %s] is not sufficient" %(useSmoothedRes))
        return False
        
    xElems, yElems = maps['h'].shape
    #xMax = maps['x'][-1]
    
    s_mean_tot = maps['s'].sum() / (xElems * yElems)
    
    var = 0 # variance
   
    for iy in range(yElems):
        s_mean_yLine = maps['s'][:,iy].sum() / xElems
#        print("y-line: %d, p_mean = %f" %(iy, s_mean_yLine))
        var += (s_mean_yLine - s_mean_tot)**2
    
    rmse  = np.sqrt(var)
    nrmse = rmse / s_mean_tot     
    
    print("==> overall, p_mean = %f" %(s_mean_tot))
    print("Var=%f, RMSE=%f, nRMSE=%f" %(var, rmse, nrmse))
    
    # return statement    
    return s_mean_tot

'''
===============================================================================
                   Calc total resistance
===============================================================================
'''
def calcMeanArealRes(allRegs, useSmoothedRes = False):
        
    pD = allRegs.pD
    
    # get arrays
    RY_arr,       _ = calc_map_R(      allRegs, axis='y')
    pot_arr,      _ = calc_map_elPot(  allRegs)
    curDensY_arr, _ = calc_map_curDens(allRegs, axis='y')
    
    # bpp potential difference
    V_bot = pot_arr[0, 0]
    V_top = pot_arr[0,-1]
    
    # nr. of elements in x and y
    xElems, yElems = RY_arr.shape
    
    # lenght of one element in x and y
    lenElemX = allRegs.xMax() / (xElems-1)
    lenElemY = pD['H_mea']    / (yElems-1)
   
    # mean y-current density
    curDensY_mean_tot = np.mean(curDensY_arr)
    
    # initiation of variance
    var = 0
    
    # potential difference vector
    for iy in range(yElems):
        curDensY_mean_yLine = np.mean(curDensY_arr[:,iy])
#        print("y-line: %d, i_mean = %f" %(iy, curDensY_mean_yLine))
        var += (curDensY_mean_yLine - curDensY_mean_tot)**2
    
    rmse  = np.sqrt(var)
    nrmse = rmse / curDensY_mean_tot     
    
    print("==> overall, i_mean = %f" %(curDensY_mean_tot))
    print("Var=%f, RMSE=%f, nRMSE=%f" %(var, rmse, nrmse))
 
    MeanArealRes = (V_top - V_bot) / curDensY_mean_tot
    print("=> Overal ArealRes = %f Ohm.mym" %(MeanArealRes))
    
    # return statement    
    return MeanArealRes



'''
===============================================================================
                   Return max u,s,t-parameters
===============================================================================
'''

def calc_max_yDisp(allRegs, useSmoothedRes = False):
    "returns the absolut maximum value of displacement in y-direction"
    if useSmoothedRes:
        return np.max(np.abs(allRegs.maps_smoothed['u']))
    else:
        return np.max(np.abs(allRegs.maps_hom['u']))


def calc_max_yStress(allRegs, useSmoothedRes = False):
    "returns the absolut maximum value of normal stress in y-direction"
    if useSmoothedRes:
        return np.max(np.abs(allRegs.maps_smoothed['s']))
    else:
        return np.max(np.abs(allRegs.maps_hom['s']))


def calc_max_ShearStress(allRegs, useSmoothedRes = False):
    "returns the absolut maximum value of shear stress"
    if useSmoothedRes:
        return np.max(np.abs(allRegs.maps_smoothed['t']))
    else:
        return np.max(np.abs(allRegs.maps_hom['t']))