# -*- coding: utf-8 -*-
"""
Created on Thu Mar 24 19:01:45 2022

@author: kohrn_simu
"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt

''' import standard libraries'''                       
import math, time, os
import numpy               as np
import matplotlib.pyplot   as plt
import matplotlib          as mpl
import copy
from scipy                 import optimize
from scipy.interpolate     import interp1d
from scipy.ndimage.filters import gaussian_filter1d

''' imoprt own function classes '''

# swith cwd to parent directory, if not yet done
if os.path.split(os.getcwd())[1] == "AnalytModel":
    os.chdir("..")

import AnalytModel.AM2_calcSingleReg           as c_calcReg
import AnalytModel.AM2_mapRegimes              as c_mapRegs
import AnalytModel.AM2_postprocessing          as c_post
import AnalytModel.AM2_calcGenericReg          as c_genReg
#import AnalytModel.AM2_main                    as AM2_main


''' Globals '''
#gEPS           = AM2_main.glob_EPS # distance of boundaries after jump
#gNrOfLines     = AM2_main.glob_nrOfLines
#gNrOfPointsReg = AM2_main.glob_nrOfPointsReg

'''
===============================================================================
                    Class: Regime
===============================================================================
'''

class Regime:

#============= Constructor ====================================================
    def __init__(self, pD, regime, idx, lBdy, rBdy):
        
        # overall
        self.pD    = pD # paraDict
        
        # regime related
        self.idx   = idx
        self.Bot   = regime[1]        
        self.Top   = regime[2]
        
        # boundaries
        self.lBdy  = lBdy
        self.rBdy  = rBdy

        # booleans
        self.final = False # if True: no further calculation
        
        # maps
        self.maps  = {"x" : None,
                      "h" : None,
                      "u" : None,
                      "s" : None,
                      "t" : None}
        
        # regime is part of a gap, else None
        self.gap        = None
        self.maps_uncor = None
        
        # correctionDict
        self.cD = None
            
                     

#============= getter Fkts ====================================================
        
    def xStart(self):
        return self.lBdy.x
        
    def xEnd(self):
        return self.rBdy.x
    
    def B(self):
        return self.Bot
    
    def T(self):
        return self.Top
    
    def BT(self):
        return self.B() + self.T()
    
    def xLen(self):
        return self.xEnd() - self.xStart()

#============= return h-value of Top/Bottom at left/right regime boundary =====
    
    def h_bdy(self, LR):
        "returns height at (top, bottom) on left or right regime boundary"
        
        # safety check
        if not self.final:
            print("Error: Regime (idx=%d) is not yet final!" %self.idx)
            return False
        
        if   LR == "left"  or LR == "L":
            h_B = self.maps["h"][ 0, 0]  # TODO: check if correct
            h_T = self.maps["h"][ 0,-1]
        elif LR == "right" or LR == "R":
            h_B = self.maps["h"][-1, 0]
            h_T = self.maps["h"][-1,-1]
            
        return h_B, h_T


#============= convert raw regime name to calc. specifications ================
    
    def get_inverted_flipped_baseReg(self):
        """
        return (Bool, Bool, String)
        Bool:   True, if regime is inverted,
        Bool:   True, if regime is flipped
        String: name of the base regime 
        """
        BT         = self.BT()
        BT_flipped = self.T() + self.B()
        
        
        if   BT in ["FF", "FR", "FO", "RR", "R-R", "RO", "-RO", "OO"]: # 8   std
            return False, False, BT
             
        elif BT in ["F-R", "F-O", "-R-R", "-R-O", "-O-O"]:             # 5  inverted
            return True, False, BT.replace("-","")     

        elif BT in ["RF", "OF", "-RR", "OR", "O-R"]:                   # 5  flipped
            return False, True, BT_flipped            

        elif BT in ["-RF", "-OF", "-O-R", ]:                           # 3  flipped + inverted
            return True, True, BT_flipped.replace("-","")
        
        elif BT == "-OR":                                              # 1  special case
            return True, True, "-RO"
        
        elif BT == "R-O":                                              # 1  special case
            return True, False, "-RO"
        
        else:
            print("ERROR: unknown BaseReg! %s" %BT)
            exit()


    def get_baseReg(self):
        "True, if the regime is inverted, False, if not"
        _, _, baseReg = self.get_inverted_flipped_baseReg()
        return baseReg
            


#============= boolean Fcts ===================================================

    def isFlipped(self):
        "True, if the regime is flipped, False, if not"
        _, isFlipped, _ = self.get_inverted_flipped_baseReg()
        return isFlipped
  
    def isInverted(self):
        "True, if the regime is inverted, False, if not"
        isInverted, _, _ = self.get_inverted_flipped_baseReg()
        return isInverted
    
    def isFinal(self):
        "True, if both boundaries are fixed and regime is fully defined"
        return self.final

    def noOpenSide(self):
        "True, if regime has no OPEN side at top or bottom"
        if self.BT().count("O") == 0:
            return True
        else:
            return False       

#============= return h-value of Top/Bottom at left/right regime boundary =====
        
    def isSolveable(self):
        "True, if regime is yet solveable, wrapper function for how solveable"
        if self.howSolveable() == False:
            return False
        else:
            return True
            
        
    
    def howSolveable(self):

        inverted, flipped, baseReg = self.get_inverted_flipped_baseReg()
        
        #========= regime is flat-flat ========================================
        
        if baseReg == "FF":
            return "flat"
        
        elif baseReg == "R-R":         
            return "R-R"
        
        #========= not inverted and left boundary heights are known ===========
        
        elif not inverted and self.lBdy.h_final():            
            # Bdy: OO | -O-O
            if baseReg == "OO":
                if self.rBdy.rReg.BT()=="-O-O" and self.rBdy.rReg.rBdy.h_final():
                    return "fromLeft_2sites"
            
            # Bdy: OF | -OF
            elif baseReg == "OF" and self.rBdy.rReg.BT() == "-OF":
                if self.rBdy.rReg.rBdy.h_final():
                    return "fromLeft_2sites"
            
            else:
                return "fromLeft"
        
        #========= inverted and right boundary heights are known ==============
        
        elif     inverted and self.rBdy.h_final():            
            # Bdy: OO | -O-O (from left)
            if baseReg == "OO":
                if self.lBdy.lReg.BT()=="OO"   and self.lBdy.lReg.lBdy.h_final():
                    return "fromRight_2sites"
             
            # Bdy: OF | -OF (from Left)
            elif baseReg == "OF" and self.lBdy.lReg.BT() == "OF":
                if self.lBdy.lReg.lBdy.h_final():
                    return "fromRight_2sites"
                
            else:
                return "fromRight"



        # Default return: if no case is selected, the regime cannot yet be solved.
        return False


#============= solve regime ===================================================
 
    def trySolve(self, Del_h0, modStr=None, noBdyAdjust=False, modStr_gapCorr=""):
        
        # regime is not solveable
        if not self.isSolveable():
            return False
        
        # regime is solveable
        howToSolve        = self.howSolveable()
        inverted, flipped, baseReg = self.get_inverted_flipped_baseReg()
        
        
        # no bdy adjustment is performed (mostly for debugging purposses)
        if noBdyAdjust:
            bdyIsAdjusted = True
            
        
        elif howToSolve == "flat":
            # no boundary needs to be adjusted
            bdyIsAdjusted = True

#====================== TESTS
        # TODO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Ugly Workaround!!!
        elif self.BT()=="-O-O" and self.gap != None:
            bdyIsAdjusted = True
            
        # TODO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Ugly Workaround!!!
        elif self.BT()=="OO" and self.gap != None:
            bdyIsAdjusted = True
#====================== TESTS
            
        elif "fromLeft" in howToSolve:
            # adjust right boundary
            bdyIsAdjusted, infoDict = self.rBdy.adjustBdy(howToSolve, Del_h0)
            
            # TODO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Ugly Workaround!!!
            if self.BT()=="FO" and self.rBdy.rReg.BT == "O-F":
                bdyIsAdjusted == True
                  
        elif "fromRight" in howToSolve:
            # adjust left boundary
            bdyIsAdjusted, infoDict = self.lBdy.adjustBdy(howToSolve, Del_h0)

            # TODO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Ugly Workaround!!!
            if self.BT()=="F-O" and self.lBdy.lReg.BT == "OF":
                bdyIsAdjusted == True

        elif howToSolve == "R-R":
            
            # adjust left boundary            
            if not modStr_gapCorr == "backward":
                bdyIsAdjusted_R, infoDict = self.rBdy.adjustBdy("R-R_fromLeft", Del_h0)
            else:
                bdyIsAdjusted_R, infoDict = True, None
            
            # adjust right
            if not modStr_gapCorr == "forward":
                bdyIsAdjusted_L, infoDict = self.lBdy.adjustBdy("R-R_fromRight", Del_h0)
            else:
                bdyIsAdjusted_L, infoDict = True, None
            
            # both adjusted?
            bdyIsAdjusted = bdyIsAdjusted_L and bdyIsAdjusted_R
            howToSolve    = "R-R_fromLeft" # direction should not be importand, so one is picked
        
        # adjustment was sucessfull -> True
        if bdyIsAdjusted:
            
            # solve regime
            self.calcReg(howToSolve, baseReg, inverted, flipped, Del_h0, modStr=modStr)
            
            # set flag to final
            self.final = True
            
            return True, None

        # adjustment was not sucessfull -> False
        else:
            # boundary jump is required
            if infoDict is not None:
                print("Boundary jump is required")
                return False, infoDict # dictionary with jump and stat bdy            
            # adjustment was not sucessfull    
            else:
                print("adjustment not sucessfull, regime not final")
                return False, None # dictionary with jump and stat bdy
        
        
    
#============= Calculate regime     ======================================    
    
    def calcReg(self, howToSolve, baseReg, inverted, flipped, Del_h0, modStr=None):
        
        print(howToSolve)
        
        xLen  = self.xLen()
        idx   = self.idx
        pD    = self.pD
        
        if howToSolve == "flat":
            h0_B  = -(pD["H_mea"] - Del_h0)/2 
            h0_T  =  (pD["H_mea"] - Del_h0)/2
            
            x_off = {"B":0, "T":0}
        
        elif "fromLeft" in howToSolve:
            h0_B, h0_T = self.lBdy.h_bdy(Del_h0)
            x_off      = self.lBdy.x_off("BT")
        
        elif "fromRight" in howToSolve:
            h0_B, h0_T = self.rBdy.h_bdy(Del_h0)
            x_off      = self.rBdy.x_off("BT")
        
        
        # select simulation mode: reduced/coupledSim/full
        if modStr == "calcReduced":
            simuMode = "reduced"
        elif modStr == "coupledSim":
            simuMode = "coupledSim"
        else:
            simuMode = "full"

        # perform simulation
        xVec, yVec, tMap, eMap, sMap, uMap, hMap, dtdxMap = c_genReg.singleRegime(self, Del_h0, xLen, h0_T, h0_B, x_off, 
                                                                                  simuMode=simuMode,
                                                                                  whatAmI="calcFinalResults")
        
        # print
        if inverted: annoInvFlp = "( invert "
        else:        annoInvFlp = "(n/invert"  
        
        if flipped:  annoInvFlp += ", flip )"
        else:        annoInvFlp += ",n/flip)"
        
        print("=> Reg-idx.%2d, BaseReg:%4s %s | xLen =%#5.1f | x_off = [%.1f, %.1f], h_B0 = %.1f, h_T0 = %.1f \n" 
              %(      idx,     baseReg,  annoInvFlp, xLen,    x_off["B"], x_off["T"], h0_B,        h0_T))
        
        
        # adjust global x-coordinate
        if inverted: # Inversion of mirrored regimes 
            xCoord = self.xEnd() - xVec
            xCoord = np.flip(xCoord)
        else:      
            xCoord = self.xStart() + xVec
        
        # update attributes
        self.maps.update({'x':xCoord, 'y':yVec, 'h':hMap,'u':uMap,'s':sMap,'t':tMap, 'dtdx':dtdxMap})
        
        return True
        
#'''
#===============================================================================
#                Functions concerning Gaps
#===============================================================================
#'''

    def needGapCorrection(self, flag_BT):
        if self.gap == None:
            return False
        else:
            return self.gap.needGapCorrection(self.idx, flag_BT)
                 
        
        
        