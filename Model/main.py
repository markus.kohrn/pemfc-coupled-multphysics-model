# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 14:24:21 2021

@author: Markus Kohrn
         RWTH Aachen University, Germany        

"""

from __future__ import division, absolute_import, \
                       print_function#, unicode_literals

# %matplotlib qt

                       
'''
===============================================================================
                    Imports
===============================================================================
'''


''' import standard libraries'''                   
from contextlib import contextmanager
import os, sys, io, tempfile
import math
import numpy             as np
import matplotlib.pyplot as plt
import matplotlib        as mpl
import multiprocessing   as mp
import copy
import time
from   scipy             import optimize
from   scipy.interpolate import interp1d
from   scipy.ndimage     import gaussian_filter1d

''' imoprt own function classes '''

import MultiPhysModel.MP_main as MP_main
import MultiPhysModel.plot    as MP_plot
import MultiPhysModel.plot_paper as MP_plot_paper
import AnalytModel.AM2_main   as AM_main
import AnalytModel.AM2_plot   as AM_plot
import class_database         as c_db

''' imoprt own function classes '''

from Parameter.class_para_base              import Parameters as c_para_base
from Parameter.class_para_base_sym          import Parameters as c_para_base_sym
from Parameter.class_para_SingleCell1_Arif  import Parameters as c_para_SingleCell1_Arif
from Parameter.class_para_SingleCell2_Hwnag import Parameters as c_para_SingleCell2_Hwnag
from Parameter.class_para_Stack_Ballard     import Parameters as c_para_Stack_Ballard

'''
===============================================================================
                    Color sets etc.
===============================================================================
'''
 

# RWTH color list
color_list12 = ["#00549F", "#E30066", "#FFED00", "#006165", "#0098A1",
                "#57AB27", "#BDCD00", "#F6A800", "#CC071E", "#A11035",
                "#612158", "#7A6FAC"]
color_list11 = ["#00549F", "#E30066", "#FFED00", "#006165", "#0098A1",
                "#57AB27", "#BDCD00", "#F6A800", "#CC071E", "#A11035", 
                "#612158"]
color_list6  = ["#00549F", "#E30066", "#FFED00", "#006165", "#0098A1",
                "#57AB27"]
                
mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=color_list11) 


DO_SUPPRESS_PRINT = True
DO_SUPPRESS_PLOTS = True


'''
===============================================================================
                    main function
===============================================================================
'''

def main():
    
    # close all old plots
    plt.close("all")

    # select parameter set
    #c_para      = c_para_base
    #c_para      = c_para_base_sym
    #c_para      = c_para_SingleCell1_Arif
    c_para      = c_para_SingleCell2_Hwnag
    #c_para      = c_para_Stack_Ballard
            
    # select Multiprocessing-Mode ["MP": multi-p. / "SP": single-p.]
    MPmode      = "SP"  
    
    # select simulation mode ["singleSim" / "UIchar"]
    SimMode     = "singleSim" #  "UIchar" # 

    # select soothing options
    trueStiff   = True
    
    # choose parameters that are to be changed between simulations
    parVarDict  = {#"p_tot"  : [3.0],# 2.5, 3.0],                # atm
                   "Del_H0"                      : [ 50   ] ,    # mym
                   #"elCon_GDL_bulk"              : [1*10**5] ,  # S/m
                   "OCV_fract"                   : [ 1.00 ] ,    # - OCV fraction
                   "mp_threshold_relDel_Max"     : [10**(-6)],
                   #"mp_threshold_curBalRel_Mean" : [ 0.001] ,    # - threshold for mean relative currend balance
                   #"mp_threshold_curBalRel_Max"  : [ 0.01 ] ,    # - threshold for max. relative current balance
                  }

    #OCV_fract_list = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3]#, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1]
    OCV_fract_list = [1.0,                  0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 
                      0.5, 0.45, 0.4]#0.35, 0.3, 0.25, 0.2, 0.15, 0.1, 0.05]


    # select plots
    doPlot = {"initRegDist" :      False ,
              "initCalc"    :      False ,
              "gapCorrect"  :      False ,
              "MP_grid"     :      False ,
              "MP_hom"      :      False ,
              "MP_smooth"   :      False ,
              "MP_res"      : True,#     False ,
              "paper"       :      False,#True,#     False ,
             }
    
    # select optional prints
    doPrint_MP         = True
    
    createNewDatabase = True#False#



#======================= pre-processing =============  
     
    paraVar_list = create_paraVar_list(parVarDict)
    paraSet_list = create_paraSet_list(c_para, paraVar_list, SimMode, OCV_fract_list)
   
    dataBase = c_db.database(createNewDatabase)

#======================= create input_data list =====  
    
    inputData_list = list()

    for paraSet in paraSet_list:
        
        # extract data from para-set
        SimMode      = paraSet[0]
        paraObj      = paraSet[1]
        OCV_fract_list = paraSet[2]
        paraDict     = paraObj.paraDict_AM
        
        # check if para-set in database:
        if dataBase.doesExist_MPres(paraObj):
            # check if all R2_loads in load-list are existing
            OCV_fract_list_old = dataBase.get_OCV_fract_list(paraObj)
            
            if OCV_fract_list == OCV_fract_list_old:
                # no calculation required
                continue
            else:
                # different load_list, treated as AMresSM
                AMres = dataBase.getAMres(paraObj)
                preKnown    = "AMresSM"
                OCV_fract_list = [R2 for R2 in OCV_fract_list if R2 not in OCV_fract_list_old]
        
        elif dataBase.doesExist_AMresSM(paraObj):
            AMres    = dataBase.getAMres(paraObj)
            preKnown = "AMresSM"
            
        elif dataBase.doesExist_AMres(paraObj):
            AMres    = dataBase.getAMres(paraObj)
            preKnown = "AMres"
            
        else:
            AMres    = None
            preKnown = "None"
            

        
        # aggredate input_data set            
        input_data = [paraDict,    paraObj,      AMres,       SimMode,      
                      doPlot,      doPrint_MP,   trueStiff,   preKnown, OCV_fract_list ]
        
        # append input_ata set to list
        inputData_list.append(input_data)
            
    print("New sets to be caltulated: %d / %d" %(len(inputData_list), len(paraSet_list)))
    time.sleep(2)

#======================= run jobs ===================

    time_start = time.time()
    print("\nStart Simulations (%s) in %s-mode\n" %(SimMode, MPmode))
    
    if MPmode=="SP":
        result_list = list()
        for inputData in inputData_list:
            result = taskHandler(inputData)
            result_list.append(result)       
    elif MPmode=="MP":
        MaxWorkers = int( min( mp.cpu_count()-1, len(inputData_list) ) )
        with mp.Pool(processes = MaxWorkers) as pool:
            result_list = pool.map(taskHandler, inputData_list)           
            pool.close()
            pool.join()    
    else:
        print("ERROR: undefined mode: %s" %MPmode)
        return False
    
    print("\nSimulation completed in TOTAL time: %d\n" %(time.time() - time_start))

#======================= store data do data base ====    

    for paraObj, AMres, MPres, OCV_fract_list in result_list:        
        dataBase.add(paraObj, AMres, MPres, OCV_fract_list)
        
#======================= end of programme ===========
    
    print("===================\n End of Programme!\n===================")
    return True




'''
===============================================================================
                    simulate Coupled Simulation
===============================================================================
'''

def simulate_coupledSim(paraDict, paraObj,    AMres,     SimMode, 
                        doPlot,   doPrint_MP, trueStiff, preKnown, OCV_fract_list):
      
    set_npError(mode='default')
    
    Del_H0 = paraObj.Del_H0
    
    timeStamp_list = [time.time()]

# === Mechanical (AM) part ===

    if preKnown == "None":
    
        # Setup all_Regimes
        regimes = AM_main.c_mapRegs.main(paraDict, doPlot=doPlot["initRegDist"])         
        allRegs = AM_main.c_allRegimes.AllRegimes(regimes, paraDict)
        allRegs.paraObj = paraObj
        allRegs.print_regimes()
                  
        # perform calculation loop

        AM_main.calculationLoop(allRegs=allRegs, Del_h0=Del_H0, 
                                plotRes = doPlot["initCalc"])
        
        # perform correction loop -> gap closure
        timeStamp_list.append(time.time())
        AM_main.correctionLoop(allRegs=allRegs, Del_h0=Del_H0, 
                                plotRes = doPlot["gapCorrect"], extraPlots = False)
    else:
        timeStamp_list.append(time.time())        
        allRegs = AMres
        
   
# === Smooth and Prepare ===
    
    if preKnown == "AMresSM":
        timeStamp_list.append(time.time())
    
    else:
        
        # TODO: NOT WORKING AT THE MOMENT!!!!===============================================================================
        
        # TODO: here all changed smoothing factors need to be transferred to the new dataset!!!!!
        
        # prepare parameters for multi-physics simulation ( incl. hom + smooth)
        timeStamp_list.append(time.time())
        prepForMP(allRegs=allRegs, paraObj=paraObj, Del_H0=Del_H0, trueStiff=trueStiff, 
                  doPlot_MP_grid=doPlot["MP_grid"], doPlot_MP_hom=doPlot["MP_hom"],
                  doPlot_MP_smooth=doPlot["MP_smooth"])
        

# === Multi-pyhsics part ===    

    # run multi-physics simulation
    timeStamp_list.append(time.time())
    set_npError(mode='allRaise')  
    MPres_dict = MP_main.main(mode=SimMode, para=paraObj, OCV_fract_list=OCV_fract_list, 
                              doSuppressPrint=not doPrint_MP, doSuppressPlots=not doPlot["MP_res"])

   
    # plot for paper
    # if doPlot["paper"]:
    #     MP_plot_paper.plot_paper(MPres_dict)
    
    # print summary
    print_summary(timeStamp_list)

    return paraObj, allRegs, MPres_dict, OCV_fract_list
    


'''
===============================================================================
                    prepare for multi-physics model
===============================================================================
'''

def prepForMP(allRegs, paraObj, Del_H0, trueStiff,
              doPlot_MP_grid, doPlot_MP_hom, doPlot_MP_smooth):
    
    # update parameter object: base geometry
    paraObj.update_geoDict(mode="coupledSim", allRegs=allRegs, doPlot=doPlot_MP_grid)
    
    # solve analyt. moodel for y-grid of multi-physics model
    for reg in allRegs.reg_list:
        
        reg.geoDict = paraObj.geoDict
        
        howToSolve        = reg.howSolveable()
        if howToSolve == "R-R":
            howToSolve +="_fromLeft"
        inverted, flipped, baseReg = reg.get_inverted_flipped_baseReg()
        
        with suppress_stdout():
            reg.calcReg(howToSolve, baseReg, inverted, flipped, Del_H0, modStr="coupledSim")
    
    # plot original (gap-loased) for multi-physics model
    AM_plot.plot_fromPlotList(allRegs, Del_H0, mode="coupledSim", winTitle="Fig. 1 - MP grid")
    

    # homogenize
    AM_main.homogenization(allRegs=allRegs, Del_h0=Del_H0, mode="coupledSim", 
                           plotRes = doPlot_MP_hom)

    # smooth
    AM_main.smoothing(allRegs, Del_H0, smoothMethod_list=["physics"], trueStiff=trueStiff,
                      plotRes = doPlot_MP_smooth, plotAdditional = False)
    
    # update parameter object: mechanical and geometrical attributes
    s_nodes = allRegs.maps_hom["s"] *10**6
    paraObj.update_all(s_nodes)
    
    paraObj.plot_Vol_and_Por()
     
    # return success
    return True


'''
===============================================================================
                    create parameter lists
===============================================================================
'''

def create_paraVar_list(inputDict):
    "combines each value with each other value from imput dict"
    returnList = [{}]    
    for key, val_list in inputDict.items():
        returnList_new = list()
        for val in val_list:
            returnList_copy = copy.deepcopy(returnList)
            for entry in returnList_copy:
                entry.update({key : val})
                returnList_new.append(entry)
        returnList = returnList_new   
    return returnList


#==============================================================================
def create_paraSet_list(c_para, paraVar_list, SimMode, OCV_fract_list):
    "STF"
    if SimMode == "singleSim":
        OCV_fract_list = None
    else:
        OCV_fract_list = OCV_fract_list
    
    paraSet_list = list()
    
    with suppress_stdout():    
        for paraVar_dict in paraVar_list:
            
            if "p_tot" in paraVar_dict:
                paraVar_dict.update({"p_tot_a": paraVar_dict["p_tot"]*101325,
                                     "p_tot_c": paraVar_dict["p_tot"]*101325})
                del paraVar_dict["p_tot"]
     
            if "R_RIB" in paraVar_dict:
                paraVar_dict.update({"R_RIBa": paraVar_dict["R_RIB"]* 10**(-6),
                                     "R_RIBc": paraVar_dict["R_RIB"]* 10**(-6)})
                del paraVar_dict["R_RIB"]
                
                
                        
            paraSet = c_para(paraVar_dict)
            paraSet_list.append((SimMode, paraSet, OCV_fract_list))
    
    return paraSet_list



'''
===============================================================================
                    helper functions
===============================================================================
'''
    
def taskHandler(args):
    return simulate_coupledSim(*args)
    
    
    # return MP_main.main(*args, 
    #                     doSuppressPrint = DO_SUPPRESS_PRINT, 
    #                     doSuppressPlots = DO_SUPPRESS_PLOTS)


#==============================================================================
@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:  
            yield
        finally:
            sys.stdout = old_stdout


#==============================================================================
def set_npError(mode):
    if mode == "default":
        np.seterr(divide='warn', over='warn',under='ignore',invalid='warn')
    if mode == "allRaise":
        np.seterr(all='raise')
        
        
#==============================================================================
def print_summary(tS):
    
    tS.append(time.time())
    
    print("\n=================================================")                  
    print(">>>>>>>>>>>> Set Simulation Summary <<<<<<<<<<<<<")
    print("=================================================\n") 

    print("   initial calculation      - %4d sec " %(tS[1]-tS[0]) )
    print("+  gap correction           - %4d sec " %(tS[2]-tS[1]) )
    print("+  smoothing                - %4d sec " %(tS[3]-tS[2]) )
    print("+  multi-physics simulation - %4d sec " %(tS[4]-tS[3]) )
    print("======================================"                )
    print("  overall                   - %4d sec " %(tS[4]-tS[0]) )              

    return True

'''
===============================================================================
                    Start-up Routine
===============================================================================
'''
if __name__ == "__main__":
    res = main()